import ReactGA from "react-ga";
import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import styled from "styled-components";
import Navbar from "./Components/Navbar";
import Home from "./Views/Home";
import Articles from "./Views/Articles";
import About from "./Views/About";
import Donate from "./Views/Donate";
import Post from "./Views/Post";
import AdminEditor from "./Views/AdminEditor";
import Search from "./Views/Search";
import Validate from "./Views/Validate";
import Firebase from "./Backend/Firebase";
import Login from "./Views/Login";
import NotFound from "./Views/NotFound";

/**
 * TODO:
 * - consider using the componentWillReceiveProps to grab the editable variable instead
 * of passing the mutator function to the Login compoenent.
 */

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			editable: false
		};

		this.database = Firebase.instance;
		ReactGA.initialize("UA-127382575-1"); //initialize Google Analytics
	}

	/**
	 * this function will allow child props to modify the editable state of this component.
	 * the editable state is inverted each time the function is called.
	 */
	toggleAdminControls = () => {
		this.setState({
			editable: !this.state.editable
		});
	};

	render() {
		return (
			<Container>
				<NavContainer>
					<Navbar
						editable={this.state.editable}
						toggleAdminControls={this.toggleAdminControls}
						{...this.props}
					/>
				</NavContainer>

				<BodyContainer>
					<Switch>
						<Route
							exact
							path="/"
							render={props => <Home editable={this.state.editable} {...props} />}
							onUpdate
						/>
						<Route
							exact
							path="/articles"
							render={props => <Articles editable={this.state.editable} {...props} />}
						/>
						<Route
							exact
							path="/about"
							render={props => <About editable={this.state.editable} {...props} />}
						/>
						<Route
							exact
							path="/donate"
							render={props => <Donate editable={this.state.editable} {...props} />}
						/>
						<Route
							exact
							path="/posts/:postID"
							render={props => <Post editable={this.state.editable} {...props} />}
						/>
						<Route
							exact
							path="/editor"
							render={props => (
								<AdminEditor
									id="tinyeditor"
									editable={this.state.editable}
									// onEditorChange={content => console.log("parent passing func to child")}
									{...props}
								/>
							)}
						/>
						<Route exact path="/search" render={props => <Search {...props} />} />
						<Route
							exact
							path="/verify/:token"
							render={props => <Validate editable={this.state.editable} {...props} />}
						/>
						<Route
							exact
							path="/login"
							render={props => <Login toggleAdminControls={this.toggleAdminControls} {...props} />}
						/>

						<Route component={NotFound} />
					</Switch>
				</BodyContainer>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	min-height: -webkit-fill-available;

	background: ${props => props.theme.homeFG};

	font-family: "Open Sans", sans-serif;

	animation-duration: 0.4s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const NavContainer = styled.div`
	flex: 0 0 auto;
`;

const BodyContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	background-color: ${props => props.theme.homeFG};

	flex: 1 1 auto;
`;

export default App;
