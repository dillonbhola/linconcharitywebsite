/**
 * @class Cloud storage with Firebase.
 *
 * The Firebase class follows the Singleton design pattern which
 * means that there can only be one instance of the Firebase
 * object in the application.
 *
 * The Firebase tools used are: Realtime Database, Authentication & Storage
 *
 * Firebase Authentication handles user authentication via email.
 *
 * Firebase Storage holds all data that is not text based. For this
 * application, it is used to store user photos.
 *
 * Firebase Realtime Database stores text based data as JSON objects.
 *
 */

import ReactGA from "react-ga";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

const cloudSingleton = Symbol(); //this is used as a key for fetching the Firebase instance
const cloudSingletonEnforcer = Symbol(); //this is used as a check to enforce one instance of Firebase

/**
 * TODO:
 * - limit the amount of reads to the COUNT
 * - the amount of reads can be limited by creating local cache
 * - revisit pagination
 */

/**
 * About Date data:
 * can get current date with now = Date.now()
 * that returns an int. you can use that int to get meaningfull text
 * by using: then = new Date(now)
 * functions are now avaialable for extracting data on the date.
 */

class Firebase {
	constructor(enforcer) {
		if (enforcer !== cloudSingletonEnforcer) {
			throw new Error("Cannot construct Firebase object as Firebase class is a Singleton. Use Firebase.instance");
		}

		if (!firebase.apps.length) {
			this._initialize();
		}

		this.rootRef = firebase.database().ref();
		this.statistics = null; //used to hold the number of posts for pagination
		this.searchResults = null; //number of search results for a search query
	}

	/**
	 * The only static function of the Firebase class.
	 * This function is used to obtain the only Firebase instance.
	 * @returns {Firebase} An instance of the Firebase class.
	 */
	static get instance() {
		if (!this[cloudSingleton]) {
			this[cloudSingleton] = new Firebase(cloudSingletonEnforcer);
		}

		return this[cloudSingleton];
	}

	/**
	 * Get a key for storing user data. The key is generated based on Firebase server time.
	 * @returns {String} String of random characters
	 */
	get pushID() {
		return this.rootRef.push().key;
	}

	/**
	 * Get the user that is currently logged in with Firebase Authenticate.
	 * This application is set up to use the user's email as their unique ID.
	 * @returns {String} User email
	 */
	get currentUser() {
		return firebase.auth().currentUser.email;
	}

	/**
	 * Add/Register a user with Firebase Authentication.
	 * @param {Object} user = {userID, password}
	 * @returns {firebase.Promise containing non-null firebase.User}
	 */
	createUser(user) {
		return firebase.auth().createUserWithEmailAndPassword(user.userID, user.password);
	}

	/**
	 * Authenticate a user with Firebase Authentication.
	 * @param {Object} user = {userID, password}
	 * @returns {firebase.Promise containing non-null firebase.User}
	 */
	loginUser(user) {
		return firebase.auth().signInWithEmailAndPassword(user.userID, user.password);
	}

	/**
	 * Logout a user with Firebase Authentication.
	 * @returns {firebase.Promise containing void}
	 */
	logoutUser() {
		return firebase.auth().signOut();
	}

	/**
	 * Adds a blog post to the realtime database. It accepts the content for a post,
	 * then adds a date created. When a new post is created, the total count of all
	 * posts in the database is updated.
	 * @param {Object} content content = {title, body}
	 * @returns {firebase.Promise containing String} String containing the ID of the post created
	 */
	createPost(content) {
		let updates = {};
		let postID = this.pushID;

		//building the post object
		let post = {};
		post = content;
		post.date_created = Date.now();

		updates[`posts/${postID}`] = post;

		if (!this.statistics) {
			// update post count
			return Promise.reject("Error: statistics object not found");
		}
		updates[`statistics/postCount`] = ++this.statistics.postCount;

		return this.rootRef.update(updates).then(() => {
			return postID;
		});
	}

	/**
	 * Updates the contents of an old post, adds an edited field to the post, and
	 * updates the post in the realtime database.
	 * @param {Object} oldPost = {title, body, comments, date_created} Original post
	 * @param {Object} content = {title, body} New post
	 * @param {String} postID The ID of the post to update
	 * @returns {firebase.Promise containing String}
	 */
	updatePost(oldPost, content, postID) {
		let updates = {};
		let editID = this.pushID;

		//building the post object
		let post = oldPost;

		post.title = content.title;
		post.body = content.body;

		if (!post.edited) post.edited = {}; //create an edited property if it never existed
		post.edited[editID] = Date.now();

		updates[`posts/${postID}`] = post;

		return this.rootRef.update(updates).then(() => {
			return "Post update successful";
		});
	}

	/**
	 * Adds a comment to a post. The comment is added to a comments property of the post object.
	 * @param {String} postID The ID of the post
	 * @param {Object} comment = {username, value}
	 * @returns {firebase.Promise containing Object} result = {data, commentID}
	 */
	createComment(postID, comment) {
		let updates = {};
		let commentID = this.pushID;

		let data = {};
		data = comment;
		data.date_created = Date.now();

		updates[`posts/${postID}/comments/${commentID}`] = data;

		return this.rootRef.update(updates).then(() => {
			let result = {};
			result.data = data;
			result.commentID = commentID;
			return result;
		});
	}

	/**
	 * Fetches the count of posts in the database and updates the local variable 'statistics'
	 * @returns {firebase.Promise containing String} string containing the number of posts in the database
	 */
	postsCount() {
		let ref = this.rootRef.child("statistics");

		return ref.once("value").then(snapshot => {
			if (!snapshot.val()) return Promise.reject("Post counter missing");

			this.statistics = snapshot.val();

			return snapshot.val();
		});
	}

	/**
	 * Fetches the post from the database given the post ID
	 * @param {String} postID The ID of the post
	 * @returns {firebase.Promise containing Object} post = {title, body, date_created, ?comments, ?edited}
	 */
	post(postID) {
		let currentRef = this.rootRef.child(`posts/${postID}`);

		return currentRef.once("value").then(snapshot => {
			return snapshot.val();
		});
	}

	/**
	 * Fetches all the posts in the database. Four posts are returned
	 * and are sorted in ascending order by the date created (oldest to newest).
	 * @returns {firebase.Promise containing Object} data = {post1, post2, post3, post4}
	 */
	get posts() {
		let data = {};
		let currentRef = this.rootRef.child("posts");

		return currentRef
			.orderByChild("date_created")
			.limitToLast(4)
			.once("value")
			.then(snapshot => {
				if (!snapshot.val()) return null;

				data = snapshot.val();
				return data;
			});
	}

	/**
	 * Grabbing the 'last n posts' in the db.
	 * Ends the read at the first id of the last post in the 'last n posts' list,
	 * then subtract n from that index, and begin the access from there.
	 * @param {String} postID The ID of the post
	 * @returns {firebase.Promise containing Object} data = {post1, post2, post3, post4}
	 */
	loadMorePosts(postID) {
		let data = {};
		let currentRef = this.rootRef.child("posts");

		return currentRef
			.orderByChild("date_created")
			.endAt(postID)
			.limitToLast(5)
			.once("value")
			.then(snapshot => {
				if (!snapshot.val()) return null;

				data = snapshot.val();
				return data;
			});
	}

	/**
	 * Deletes a post from the database given the post ID. Also updates the total number
	 * of posts in the database.
	 * @param {String} postID The ID of the post
	 * @returns {firebase.Promise containing String}
	 */
	deletePost(postID) {
		let updates = {};

		updates[`posts/${postID}`] = null;
		if (!this.statistics) {
			return Promise.reject("Error: statistics object not found");
		}
		updates[`statistics/postCount`] = --this.statistics.postCount;

		return this.rootRef.update(updates).then(() => {
			//checking if the post is featured
			return this._getFeaturedPostId()
				.then(result => {
					if (postID === result) {
						return this.updateFeaturedPost(postID, null);
					}
				})
				.then(() => {
					return "Post successfully deleted";
				});
		});
	}

	/**
	 * Deletes a comment from a post given the comment's ID and the post ID.
	 * @param {String} postID The ID of the post
	 * @param {String} commentID The ID of the comment
	 * @returns {firebase.Promise containing String}
	 */
	deleteComment(postID, commentID) {
		let updates = {};

		updates[`posts/${postID}/comments/${commentID}`] = null;

		return this.rootRef.update(updates).then(() => {
			return "Comment successfully deleted";
		});
	}

	/**
	 * This function accepts the query entered by the user and builds an object with it. The
	 * object is then added to the database under the 'search/request' property.
	 * The elastic search server listens to the 'search/request' endpoint in the realtime database
	 * and processes the query. The server then writes the results of the search to the 'search/response'.
	 * This fuction attaches a listener to the 'search/response' endpoint where the function showResults is
	 * executed.
	 * @param {String} query The search string entered by user
	 */
	search(query) {
		let data = {
			q: query + "*",
			index: "firebase",
			type: "post"
		};

		let currentRef = this.rootRef.child("search");
		var key = currentRef.child("request").push(data).key;
		// console.log("search", key, data);

		currentRef.child("response/" + key).on("value", this.showResults);
	}

	/**
	 * This function removes the listener from the parent and stores the results
	 * in the local variable 'searchResults'.
	 */
	showResults = snap => {
		if (!snap.exists()) {
			return;
		} // wait until we get data
		var dat = snap.val().hits;

		// when a value arrives from the database, stop listening
		// and remove the temporary data from the database
		snap.ref.off("value", this.showResults);
		snap.ref.remove();

		this.searchResults = dat;
	};

	/**
	 * Returns the local variable 'searchResults'
	 */
	getSearchData() {
		if (!this.searchResults) return;
		else {
			let result = this.searchResults;
			this.searchResults = null; //reseting search results
			return result;
		}
	}

	/**
	 * Updates the 'featured/post' endpoit, which holds the featured post.
	 * @param {String} postID The ID of the post
	 * @param {Object} postData = {title, body, date_created, ?comments, ?edited}
	 * @returns {firebase.Promise containing String}
	 */
	updateFeaturedPost(postID, postData) {
		let updates = {};

		//building the post object
		let post = null;
		if (postData !== null) {
			post = postData;
			post.postID = postID;
		}

		updates[`featured/post`] = post;

		return this.rootRef.update(updates).then(() => {
			return "Featured post update successful";
		});
	}

	/**
	 * Fetches the post from the 'featured' endpoint
	 * @returns {firebase.Promise containing Object} post = {title, body, date_created, postID, ?comments, ?edited}
	 */
	getFeaturedPosts() {
		let data = {};
		let currentRef = this.rootRef.child("featured");

		return currentRef.once("value").then(snapshot => {
			if (!snapshot.val()) return null;

			data = snapshot.val();
			return data;
		});
	}

	/***
	 * TODO:
	 * CHECK IF THE EMAIL ENTERED IS ALREADY A SUBSCRIBER
	 * https://example-95c49.firebaseio.com/emails.json
	 * ?orderBy="$value"&startAt="dam@mail.com"&endAt="dam@mail.com"
	 */

	verifyEmail(token) {
		let updates = {};
		//this executes when the user clicks the link in the email
		let currentRef = this.rootRef.child(`verify`).child(token);
		return currentRef.once("value").then(snapshot => {
			if (!snapshot.val()) return "This token does not exist"; //tokenID does not exist

			let data = snapshot.val();

			//check if the token has expired
			let present = new Date();
			let past = new Date(data.date_created);
			let elapsedTime = (present - past) / 86400000; //should get days
			console.log("Firebase: elaspsed time is: " + elapsedTime);

			if (elapsedTime >= 1) {
				//expired
				//removing the token from the verify list
				updates[`verify/${token}`] = null;
				return this.rootRef.update(updates).then(() => {
					return "Token expired, email removed from pending verification list.";
				});
			} else {
				//user has successfully confirmed their email
				let userID = this.pushID;
				//add them to the new list
				updates[`subscribers/${userID}`] = {
					date_created: Date.now(),
					email: data.email
				};
				//remove them from the old list
				updates[`verify/${token}`] = null;
			}

			return this.rootRef.update(updates).then(() => {
				return "New Subscriber added!";
			});
		});
	}

	subscribe(email) {
		/**
		 * TODO: perform email check here
		 * -return value should be easy to compare.
		 *  or can return a Promise.reject('string')
		 */

		let updates = {};

		let token = this._generateToken();
		// console.log("Firebase: the token is: " + token);

		updates[`verify/${token}`] = {
			email: email,
			date_created: Date.now()
		};

		return this.rootRef.update(updates).then(() => {
			// console.log("Success: subscriber pending confirmation");
			return token;
		});
	}

	/**
	 * Adds a photo to the firebase storage under the 'images/$imageID' endpoint.
	 * The URL to access the photo in storage is returned.
	 * @param {Object} file
	 * @returns {firebase.Promise containing String} URL for image
	 */
	uploadPhoto(file) {
		let imageID = this.pushID;
		const imageRef = firebase
			.storage()
			.ref()
			.child("images")
			.child(imageID);

		return imageRef.put(file).then(result => {
			return imageRef.getDownloadURL();
		});
	}

	/**
	 * Helper function.
	 * Initializes Firebase.
	 */
	_initialize() {
		firebase.initializeApp({
			apiKey: "AIzaSyBFQGzt0w73iz7jl4ang2LV23snP1r6MIo",
			authDomain: "example-95c49.firebaseapp.com",
			databaseURL: "https://example-95c49.firebaseio.com",
			projectId: "example-95c49",
			storageBucket: "example-95c49.appspot.com",
			messagingSenderId: "293451436410"
		});
	}

	/**
	 * Helper function.
	 * Initializes Google Analytics.
	 */
	_initializeReactGA() {
		ReactGA.initialize("UA-127382575-1");
	}

	/**
	 * Helper function.
	 * Firebase keys cannot contain the following characters: . # $ / [ ] %
	 * This function converts a string to an acceptable format for Firebase Realtime Database.
	 * @param {String} keyword The user email to be converted
	 * @returns {String}
	 */
	// _encodeAsFirebaseKey(keyword) {
	// 	return keyword
	// 		.replace(/\%/g, "%25")
	// 		.replace(/\./g, "%2E")
	// 		.replace(/\#/g, "%23")
	// 		.replace(/\$/g, "%24")
	// 		.replace(/\//g, "%2F")
	// 		.replace(/\[/g, "%5B")
	// 		.replace(/\]/g, "%5D");
	// }

	/**
	 * Helper function.
	 * Generates a unique string of 12 characters based on day and time, similar to firebase keys.
	 */
	_generateToken() {
		// Modeled after base64 web-safe chars, but ordered by ASCII.
		let PUSH_CHARS = "+0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz@#$";

		// Timestamp of last push, used to prevent local collisions if you push twice in one ms.
		let lastPushTime = 0;

		// We generate 72-bits of randomness which get turned into 12 characters and appended to the
		// timestamp to prevent collisions with other clients.  We store the last characters we
		// generated because in the event of a collision, we'll use those same characters except
		// "incremented" by one.
		let lastRandChars = [];

		return (() => {
			let now = new Date().getTime();
			let duplicateTime = now === lastPushTime;
			lastPushTime = now;

			let timeStampChars = new Array(8);
			for (var i = 7; i >= 0; i--) {
				timeStampChars[i] = PUSH_CHARS.charAt(now % 64);
				// NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
				now = Math.floor(now / 64);
			}
			if (now !== 0) throw new Error("We should have converted the entire timestamp.");

			let id = timeStampChars.join("");

			if (!duplicateTime) {
				for (i = 0; i < 12; i++) {
					lastRandChars[i] = Math.floor(Math.random() * 64);
				}
			} else {
				// If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
				for (i = 11; i >= 0 && lastRandChars[i] === 63; i--) {
					lastRandChars[i] = 0;
				}
				lastRandChars[i]++;
			}
			for (i = 0; i < 12; i++) {
				id += PUSH_CHARS.charAt(lastRandChars[i]);
			}
			if (id.length !== 20) throw new Error("Length should be 20.");

			return id;
		})();
	}

	/**
	 * Helper Function
	 * This function uses the getFeaturedPosts function to return
	 * the featured post. The post ID is then returned.
	 * @returns {firebase.Promise containing String}
	 */
	_getFeaturedPostId() {
		return this.getFeaturedPosts().then(result => {
			if (result === null) return;

			return result.post.postID;
		});
	}
}

export default Firebase;
