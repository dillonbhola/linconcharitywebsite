import React, { Component } from "react";
import Firebase from "../Backend/Firebase";
import styled from "styled-components";
import { Link, Redirect } from "react-router-dom";
import { withRouter } from "react-router-dom";
import ConfirmBox from "../Components/ConfirmBox";

/**
 * TODO:
 * - add delete and edit firebase functionality
 * - ensure that pages refresh properly (when deleting a comment, there was no refresh)
 */

class AdminMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {
			adminControlsFlag: false,
			showDeleteModal: false,
			showFeatureModal: false,
			redirect: false,
			path: null
		};

		this.database = Firebase.instance;
	}

	handleOpenDeleteModal = () => {
		this.setState({ showDeleteModal: true });
	};
	handleCloseDeleteModal = () => {
		this.setState({ showDeleteModal: false });
	};

	handleOpenFeatureModal = () => {
		this.setState({ showFeatureModal: true });
	};
	handleCloseFeatureModal = () => {
		this.setState({ showFeatureModal: false });
	};

	handleShowAdminControls = () => {
		this.setState({
			adminControlsFlag: !this.state.adminControlsFlag
		});
	};

	/**
	 * This function responds to an onClick event.
	 * Performs the relevant delete operation based on the props passed.
	 */
	handleDelete = () => {
		if (this.props.commentID) {
			//deleteing a comment
			return this.database.deleteComment(this.props.postID, this.props.commentID).then(result => {
				this.setState({
					redirect: true,
					path: "/articles"
				});
			});
		} else if (this.props.featuredID) {
			//deleting a featured post
			return this.database.updateFeaturedPost(this.props.featuredID, null).then(result => {
				this.handleCloseDeleteModal();
				this.props.update(null);
			});
		} else {
			//deleteing a post
			return this.database
				.deletePost(this.props.postID)
				.then(result => {
					console.log(result);
				})
				.then(() => {
					this.setState({
						redirect: true,
						path: "/"
					});
				});
		}
	};

	/**
	 * This function responds to an onClick event.
	 * Sets a post as a featured post.
	 */
	handleFeature = () => {
		if (this.props.postID) {
			return this.database.updateFeaturedPost(this.props.postID, this.props.postData).then(result => {
				this.setState({
					redirect: true,
					path: "/"
				});
			});
		} else {
			console.log("Error: no postID found");
		}
	};

	/**
	 * Renders the admin controls if the adminControlsFlag in state is true.
	 */
	renderAdminControls() {
		let flag = this.state.adminControlsFlag;

		if (flag)
			return (
				<AdminControlsMenu>
					{this.props.delete ? (
						<MenuItem onClick={this.handleOpenDeleteModal}>
							<i className="material-icons">delete_forever</i>
							<MenuText>delete</MenuText>
						</MenuItem>
					) : null}
					<hr />
					{this.props.edit ? (
						<MenuItem>
							<i className="material-icons">edit</i>
							<MenuText>
								<Link
									to={{
										pathname: `/editor`,
										state: {
											postData: this.props.postData,
											postID: this.props.postID
										}
									}}
									style={{
										color: "inherit",
										textDecoration: "inherit"
									}}
								>
									edit
								</Link>
							</MenuText>
						</MenuItem>
					) : null}
					<hr />
					{this.props.featured ? (
						<MenuItem onClick={this.handleOpenFeatureModal}>
							<i className="material-icons">star</i>
							<MenuText>feature</MenuText>
						</MenuItem>
					) : null}
				</AdminControlsMenu>
			);
	}

	render() {
		//do not render this component if the admin is logged in
		if (!this.props.editable) return null;

		// redirects to the proper component while maintaining props
		if (this.state.redirect) {
			return (
				<Redirect
					push
					to={{
						pathname: this.state.path,
						editable: this.props.editable
					}}
				/>
			);
		}

		return (
			<Container>
				<AdminControlsIcon className="material-icons" onClick={this.handleShowAdminControls}>
					more
				</AdminControlsIcon>
				{this.renderAdminControls()}

				<ConfirmBox
					modalState={this.state.showDeleteModal}
					message={"Are you sure you want to delete this? (This action is irreversible)"}
					onSubmit={this.handleDelete}
					onCancel={this.handleCloseDeleteModal}
					actionText={"Delete"}
				/>

				<ConfirmBox
					modalState={this.state.showFeatureModal}
					message={"Are you sure you want to feature this post on the home page?"}
					onSubmit={this.handleFeature}
					onCancel={this.handleCloseFeatureModal}
					actionText={"Feature"}
				/>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	margin-top: 0.3em;
`;

const AdminControlsIcon = styled.i`
	cursor: pointer;
	color: ${props => props.theme.text};
`;

const AdminControlsMenu = styled.ul`
	right: 0px;
	position: absolute;
	display: block;
	width: 150px;
	border-top-right-radius: 0;
	background-clip: padding-box;
	background-color: #fff;
	border: 1px solid rgba(0, 0, 0, 0.15);
	border-radius: 3px;
	box-shadow: 0 3px 8px rgba(0, 0, 0, 0.3);
	padding: 5px 0;
	list-style-type: none;
	margin: 0;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
	-webkit-padding-start: 1%;
	-webkit-padding-end: 1%;
	margin-right: 10%;

	animation-duration: 0.1s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const MenuItem = styled.li`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	cursor: pointer;
	color: ${props => props.theme.text};
`;

const MenuText = styled.span`
	font-variant-caps: small-caps;
	margin-left: 5%;
`;

export default withRouter(AdminMenu);
