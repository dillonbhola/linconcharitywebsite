import React, { Component } from "react";
import Firebase from "../Backend/Firebase";
import styled from "styled-components";

/**
 * TODO:
 * - add a comment as annonymous checkbox that will disable the textinput onclick
 */

class CommentBox extends Component {
	constructor(props) {
		super(props);

		this.state = {
			name: null,
			comment: null
		};

		this.database = Firebase.instance;
	}

	/**
	 * This function accepts text input from a textinput field.
	 * Updates the name state.
	 */
	handleNameChange = event => {
		this.setState({
			name: event.target.value
		});
	};

	/**
	 * This function accepts text input from a textinput field.
	 * Updates the comment state.
	 */
	handleCommentChange = event => {
		this.setState({
			comment: event.target.value
		});
	};

	/**
	 * This function responds to an onClick event.
	 * The name and comment state are sent to the backend to create a comment.
	 */
	handleSubmit = event => {
		let comment = {};
		let data = this.state;

		comment.user = data.name;
		comment.value = data.comment;

		return this.database.createComment(this.props.postID, comment);
	};

	render() {
		return (
			<Container>
				<form onSubmit={this.handleSubmit}>
					<div>
						<TextInput type="text" placeholder="Name" required onChange={this.handleNameChange} />
					</div>

					<div>
						<CommentInput placeholder="Enter comment here" required onChange={this.handleCommentChange} />
					</div>

					<div>
						<CommentSubmitButton type="submit" value="Submit" />
					</div>
				</form>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	@media only screen and (max-width: 600px) {
		padding: 1em;
	}
	border-top: 1px solid ${props => props.theme.homeFG};
	background: ${props => props.theme.background};
	padding: 2em;

	animation-duration: 0.25s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const TextInput = styled.input`
	@media only screen and (max-width: 600px) {
		width: 80%;
	}
	border-radius: 6px;
	border: 1px solid ${props => props.theme.homeFG};
	padding: 0.5em 1em;
	font-size: 1rem;
	background-color: transparent;
`;

const CommentInput = styled.textarea`
	border-radius: 6px;
	border: 1px solid ${props => props.theme.homeFG};
	padding: 1em;
	font-size: 1.2em;
	width: 80%;
	margin: 1em 0;
	background-color: transparent;
`;

const CommentSubmitButton = styled.input`
	font-size: 1.2em;
	padding: 0.2em 1em;
	border: 2px solid ${props => props.theme.homeFG};
	border-radius: 6px;
	background-color: ${props => props.theme.homeFG};
	color: ${props => props.theme.background};
	cursor: pointer;
	font-variant: all-small-caps;
	transition: background-color 250ms;
`;

export default CommentBox;
