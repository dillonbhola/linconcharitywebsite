import React, { Component } from "react";
import styled from "styled-components";
import ReactModal from "react-modal";

/**
 * Mandatory Props (Must be provided or will break the application):
 * - {modalState} the parent's state from the modal.
 * - {message} message to be displayed in the modal box.
 * - {onCancel} updates the parent state to determine if the modal should be displayed.
 *              to be used by the Cancel button.
 * - {onSubmit} executes the parent function. to be used by the Save button.
 * - {actionText} the text the user will see in the Submit button.
 */

class ConfirmBox extends Component {
	render() {
		return (
			<ReactModal
				isOpen={this.props.modalState}
				contentLabel="Confirmation"
				ariaHideApp={false}
				onRequestClose={this.props.onCancel}
				shouldCloseOnEsc={true}
				style={customStyles}
			>
				<Message>
					<p>{this.props.message}</p>
				</Message>

				<ModalButtons>
					<SubmitButton onClick={this.props.onSubmit}>{this.props.actionText}</SubmitButton>
					<CancelButton onClick={this.props.onCancel}>Cancel</CancelButton>
				</ModalButtons>
			</ReactModal>
		);
	}
}

const customStyles = {
	content: {
		top: "auto",
		left: "30%",
		width: "200px",
		// right:  '20%',
		bottom: "30%",
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-evenly",
		borderColor: "black"
	}
};

/////////////////////////////////// styled components ///////////////////////////////////

const Message = styled.div`
	margin: 0 5%;
	font-size: large;
`;

const ModalButtons = styled.div`
	display: flex;
	justify-content: space-evenly;
`;

const SubmitButton = styled.button`
	font-size: 1em;
	margin: 2% 2% 2% 0%;
	padding: 0.25em 1em;
	border: 2px solid ${props => props.theme.homeFG};
	border-radius: 3px;
	background: ${props => props.theme.homeFG};
	color: white;
	cursor: pointer;
	width: 45%;
`;

const CancelButton = SubmitButton.extend`
	background: white;
	color: ${props => props.theme.homeFG};
`;

export default ConfirmBox;
