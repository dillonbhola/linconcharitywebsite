import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import ReactModal from "react-modal";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
import ConfirmBox from "./ConfirmBox";

/**
 * TODO:
 * - make navbar sticky and add onscroll transition
 */

class Navbar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showSearchModal: false,
			showMobileModal: false,
			showLogoutModal: false,
			redirectToSearch: false,
			searchQuery: null
		};

	}

	componentDidUpdate() {
		/**
		 * need to reset the redirect value here as the Navbar is not being
		 * rendered by React Router, hence it will not unmount and mount, reseting
		 * its state.
		 * If the value is not reset, its state will persist and the value of
		 * redirectToSearch will remain true.
		 */
		let check = this.state.redirectToSearch;
		if (check) {
			console.log("reset state of navbar redirect");
			this.setState({
				redirectToSearch: false
			});
		}
	}

	handleLogoutAdmin = () => {
		let database = Firebase.instance;
		database.logoutUser();
		this.props.toggleAdminControls();
		window.location.reload();
	}

	handleOpenSearchModal = () => {
		this.setState({ showSearchModal: true });
	};
	handleCloseSearchModal = () => {
		this.setState({ showSearchModal: false });
	};

	handleOpenMobileModal = () => {
		this.setState({ showMobileModal: true });
	};
	handleCloseMobileModal = () => {
		this.setState({ showMobileModal: false });
	};

	handleOpenLogoutModal = () => {
		this.setState({ showLogoutModal: true });
	};
	handleCloseLogoutModal = () => {
		this.setState({ showLogoutModal: false });
	};

	handleSearchSubmit = event => {
		event.preventDefault(); //prevent form POST and refresh of page

		let query = event.target[0].value;
		this.setState({
			redirectToSearch: true,
			searchQuery: query
		});
		this.handleCloseSearchModal();
	};

	render() {
		if (this.state.redirectToSearch) {
			return (
				<Redirect
					push
					to={{
						pathname: "/search",
						search: this.state.searchQuery
					}}
				/>
			);
		}

		return (
			<div>
				<Container className="navigation">
					<NavLink to="/"> Home</NavLink>
					<NavLink to="/articles"> Articles</NavLink>
					<NavLink to="/about">About</NavLink>
					{/* <NavLink to="/donate">Donate</NavLink> */}
					{this.props.editable ? <NavLink to="/editor">Editor</NavLink> : null}
					{this.props.editable ? <LogoutButton onClick={this.handleOpenLogoutModal}>Logout Admin</LogoutButton> : null}

					{/* <SearchContainer onClick={this.handleOpenSearchModal}>
						<i className="material-icons">search</i>
					</SearchContainer> */}

					{/* <Modal
						isOpen={this.state.showSearchModal}
						contentLabel="Search Interface"
						ariaHideApp={false}
						onRequestClose={this.handleCloseSearchModal}
						shouldCloseOnEsc={true}
						style={customStyles}
					>
						<form onSubmit={this.handleSearchSubmit}>
							<InputText type="text" placeholder="Search Articles" name="search" />
						</form>
					</Modal> */}
				</Container>

				<MobileContainer>
					<MenuIcon onClick={this.handleOpenMobileModal}>
						<i className="material-icons">menu</i>
					</MenuIcon>

					<Modal
						isOpen={this.state.showMobileModal}
						contentLabel="Mobile Interface"
						ariaHideApp={false}
						onRequestClose={this.handleCloseMobileModal}
						shouldCloseOnEsc={true}
						style={mobileStyles}
					>
						<NavLink to="/" onClick={this.handleCloseMobileModal}>
							{" "}
							Home
						</NavLink>
						<NavLink to="/articles" onClick={this.handleCloseMobileModal}>
							{" "}
							Articles
						</NavLink>
						<NavLink to="/about" onClick={this.handleCloseMobileModal}>
							About
						</NavLink>
						{/* <NavLink to="/donate" onClick={this.handleCloseMobileModal}>Donate</NavLink> */}
						{this.props.editable ? (
							<NavLink to="/editor" onClick={this.handleCloseMobileModal}>
								Editor
							</NavLink>
						) : null}
						{this.props.editable ? <LogoutButton onClick={this.handleOpenLogoutModal}>Logout Admin</LogoutButton> : null}
					</Modal>
				</MobileContainer>

				<ConfirmBox
					modalState={this.state.showLogoutModal}
					message={"Are you sure you want to logout?"}
					onSubmit={this.handleLogoutAdmin}
					onCancel={this.handleCloseLogoutModal}
					actionText={"Logout"}
				/>
			</div>
		);
	}
}

// const customStyles = {
// 	overlay: {
// 		display: "flex",
// 		justifyContent: "center",
// 		alighnItems: "center",
// 		backgroundColor: "rgba(255,255,255, 0.9)"
// 	},
// 	content: {
// 		position: "relative",
// 		top: "auto",
// 		width: "80%",
// 		textAlign: "center",
// 		marginTop: "30px",
// 		margin: "auto",
// 		padding: "0",
// 		left: "0",
// 		right: "0",
// 		border: "2px solid #000000a3"
// 	}
// };

const mobileStyles = {
	overlay: {
		display: "flex",
		justifyContent: "center",
		alighnItems: "center",
		backgroundColor: "rgba(255,255,255, 0.9)"
	},
	content: {
		position: "relative",
		top: "auto",
		width: "80%",
		textAlign: "center",
		marginTop: "30px",
		margin: "auto",
		padding: "0",
		left: "0",
		right: "0",
		display: "grid"
	}
};

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.nav`
	@media only screen and (max-width: 600px) {
		display: none;
	}
	display: flex;
	flex-direction: row;

	justify-content: space-evenly;
	align-items: center;

	padding: 1%;

	background: ${props => props.theme.homeFG};
`;

// const SearchContainer = styled.div`
// 	display: flex;
// 	cursor: pointer;

// 	animation-duration: 0.25s;
// 	animation-name: fadeIn;
// 	animation-timing-function: ease-in;
// 	color: ${props => props.theme.background};

// 	padding: 0 5%;
// `;

// const InputText = styled.input`
// 	padding: 15px;
// 	font-size: 17px;
// 	border: none;
// 	float: left;
// 	background: white;
// 	width: -webkit-fill-available;

// 	&:hover {
// 		background: ${props => props.theme.homeBG};
// 	}
// `;

const Modal = styled(ReactModal)`
	animation-duration: 0.25s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const NavLink = styled(Link)`
	@media only screen and (max-width: 600px) {
		background: ${props => props.theme.homeFG};
		padding: 6% 0;
		border: 0.5px solid;
	}

	font-size: x-large;
	text-decoration-line: none;
	color: ${props => props.theme.background};
	font-variant-caps: all-petite-caps;

	&:hover {
		font-weight: bold;
	}

	padding: 0 5%;
	letter-spacing: 5px;
`;

const MobileContainer = styled.div`
	@media only screen and (max-width: 600px) {
		display: flex;
		justify-content: center;
		align-items: center;
		background: ${props => props.theme.homeFG};
	}

	display: none;
`;

const MenuIcon = styled.div`
	display: flex;
	cursor: pointer;

	animation-duration: 0.25s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	color: ${props => props.theme.background};
	padding: 0 5%;

	i {
		font-size: 50px;
	}
`;

const LogoutButton = styled.div`
	@media only screen and (max-width: 600px) {
		background: ${props => props.theme.homeFG};
		padding: 6% 0;
		border: 0.5px solid;
		font-size: x-large;
		text-align: center;
		margin-left: initial;
	}
	cursor: pointer;

	background: crimson;
	color: white;
	font-variant-caps: all-petite-caps;
	padding: 0.5%;
	text-align: end;
	margin-left: auto;

	&:hover {
		font-weight: bold;
	}
`;

export default Navbar;
