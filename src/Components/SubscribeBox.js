import React, { Component } from "react";
import Firebase from "../Backend/Firebase";
import styled from "styled-components";

/**
 * TODO:
 * - animate entire component instead of individual parts
 */

class SubscribeBox extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: null,
			overlay: true
		};

		this.database = Firebase.instance;
	}

	handleEmailChange = event => {
		this.setState({
			email: event.target.value
		});
	};

	handleSubmit = event => {
		event.preventDefault();

		let data = this.state;
		console.log(data.email);

		return this.database.subscribe(data.email).then(result => {
			let body = {
				email: data.email,
				token: result
			};

			fetch("/api/send", {
				method: "POST",
				headers: {
					"Content-Type": "application/json; charset=UTF-8"
				},
				credentials: "include",
				body: JSON.stringify(body)
			})
				.then(response => {
					console.log(response.status);

					//checking the header of the response object to determine whether or
					//not a json object was received.
					let contentType = response.headers.get("content-type");
					if (contentType && contentType.includes("application/json")) {
						console.log("JSON object received from server");
						response.json().then(data => {
							//could return this and get data in next then
							console.log(data);
							console.log("The email has been successfully sent to the user");
						});
					} else {
						console.log("Plain Text object received from server");
						response.text().then(data => {
							console.log(data);
						});
					}
				})
				.catch(err => {
					console.log(err);
				});
		});
	};

	handleOverlayClick = () => {
		this.setState({
			overlay: false
		});
	};

	render() {
		const backgroundColor = this.state.overlay ? "trasparent" : null;
		return (
			<Container color={backgroundColor}>
				{this.state.overlay ? (
					<Overlay>
						<ClearButton onClick={this.handleOverlayClick}>Subscribe for updates</ClearButton>
					</Overlay>
				) : (
					<SubscribeContainer>
						<SubscribeForm onSubmit={this.handleSubmit}>
							<TextInput type="email" placeholder="Email" required onChange={this.handleEmailChange} />

							<SubmitButton type="submit" value="Submit" />
						</SubscribeForm>

						<SubscribeDescription>
							<p>
								By entering your email you will be able to receive updates when new articles are posted.
							</p>
						</SubscribeDescription>
					</SubscribeContainer>
				)}
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div.attrs({
	color: props => props.color || props.theme.homeFG
})`
	display: flex;
	justify-content: center;
	background-color: ${props => props.color};

	transition: background-color 200ms;
`;

const SubscribeContainer = styled.div`
	padding: 5em 2em;
	animation-duration: 0.25s;
	animation-name: slideInUp;
	animation-timing-function: ease-in;
`;

const Overlay = styled.div`
	@media only screen and (max-width: 600px) {
		flex-grow: 1;
	}
	color: ${props => props.theme.homeFG};
`;

const ClearButton = styled.div`
	@media only screen and (max-width: 600px) {
		margin: 5%;
	}
	font-size: 1.6em;
	margin: 2em;
	padding: 0.3em 1em;
	border-radius: 3px;
	background: transparent;
	color: ${props => props.theme.homeFG};
	cursor: pointer;
	font-variant: all-petite-caps;
	font-weight: bold;
	font-family: sans-serif;
	border-style: solid;
	border-color: ${props => props.theme.homeFG};
	border-width: medium;
	text-align: center;
`;

const SubscribeForm = styled.form`
	display: flex;
	justify-content: center;
	flex-flow: wrap;
`;

const TextInput = styled.input`
	margin: 2% 2% 2% 0%;
	border-radius: 2px;
	border: 1px solid ${props => props.theme.homeBG};
	background: ${props => props.theme.homeBG};
	padding: 0.3em;
	font-size: 1rem;
	flex-grow: 1;
	font-size: 1.2em;
`;

const SubmitButton = styled.input`
	@media only screen and (max-width: 600px) {
		flex-grow: 1;
	}
	font-size: 1.2em;
	margin: 2% 2% 2% 0%;
	padding: 0.25em 1em;
	border: 1px solid ${props => props.theme.homeBG};
	border-radius: 3px;
	background: ${props => props.theme.homeFG};
	color: ${props => props.theme.homeBG};
	cursor: pointer;
	flex-grow: 0;
	font-variant: all-petite-caps;
`;

const SubscribeDescription = styled.div`
	text-align: center;
	color: ${props => props.theme.homeBG};
`;

export default SubscribeBox;
