import React, { Component } from "react";
import styled from "styled-components";
import ReactGA from "react-ga";

class About extends Component {
	constructor(props) {
		super(props);

		ReactGA.pageview("/about");
	}

	render() {
		return (
			<Container>
				<Content>
					<p>The Shubhdaan Foundation was established in Trinidad and Tobago, on the 6th of March, 2008.</p>
				</Content>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	// background: transparent;
	// flex: auto;

	background: ${props => props.theme.background};

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;
	// margin-top: 3em;
	box-shadow: 0em 0em 100px 0px black;
`;

const Content = styled.div`
	background: ${props => props.theme.background};
	color: ${props => props.theme.text};

	margin: 10% 15%;
	padding: 5em 0 4em 0;

	font-size: larger;
	border-bottom: solid;
`;

export default About;
