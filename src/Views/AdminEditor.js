import React, { Component } from "react";
import styled from "styled-components";
import ReactModal from "react-modal";
import Firebase from "../Backend/Firebase";
import ConfirmBox from "../Components/ConfirmBox";
import { Redirect } from "react-router-dom";

import tinymce from "tinymce";
import "tinymce/themes/modern";
import "tinymce/plugins/wordcount";
import "tinymce/plugins/table";
import "tinymce/plugins/advlist";
import "tinymce/plugins/colorpicker";
import "tinymce/plugins/insertdatetime";
import "tinymce/plugins/link";
import "tinymce/plugins/lists";
import "tinymce/plugins/media";
import "tinymce/plugins/preview";
import "tinymce/plugins/searchreplace";
import "tinymce/plugins/textcolor";
import "tinymce/plugins/image";

import ReactGA from "react-ga";

/**
 * TODO:
 * - add upload indicator to picture upload
 * - add pick from previous uploads (should require thumbnails)
 * - add youtube button
 */

class AdminEditor extends Component {
	constructor(props) {
		super(props);

		this.state = {
			editor: null,
			preview: null,
			title: {
				opentag: "<h2>",
				content: "",
				closetag: "</h2>"
			},
			body: "",
			showPreviewModal: false,
			showSubmitModal: false,
			showUpdateModal: false,
			showImageModal: false,
			showYoutubeModal: false,
			image: "", //used when inserting an image via url
			isEditing: false, //used when editing a post
			redirect: false,
			path: null, //used for redirecting
			file: "", //contains the image file to be uploaded
			imagePreviewUrl: "", //used in the insert image modal
			link: "" //used in the youtube modal
		};

		this.database = Firebase.instance;
		ReactGA.pageview("/admineditor");
	}

	componentDidMount() {
		//redirect if admin is not logged in
		if (this.props.editable === false) {
			this.setState({
				redirect: true,
				path: "/"
			});
		}

		//initialize editor
		tinymce.init({
			selector: `#${this.props.id}`,
			skin_url: `${process.env.PUBLIC_URL}/skins/lightgray`,
			theme: "modern",
			height: "100%",
			plugins:
				"wordcount table advlist lists textcolor colorpicker insertdatetime link media preview searchreplace image",
			toolbar:
				"insert | undo redo | bold italic underline | forecolor backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat",
			target_list: false,
			branding: false,
			elementpath: false,
			content_style: "div {margin: 10px; border: 5px solid red; padding: 3px}",
			setup: editor => {
				this.setState({ editor });
				editor.on("keyup change", () => {
					const content = editor.getContent();
					// this.props.onEditorChange(content);
					this.setState({ body: content });
				});
			}
		});

		//prepare this.state if editing a post
		if (this.props.location.state) {
			let title = this.state.title;
			let body = this.state.body;

			title.content = this.props.location.state.postData.title;
			body = this.props.location.state.postData.body;

			this.setState({
				title: title,
				body: body,
				isEditing: true
			});
		}

		//ensure backend cache is up to date
		this.database.postsCount();
	}

	componentWillUnmount() {
		tinymce.remove(this.state.editor); //removing editor listener
	}

	//grabs the value from the title textarea and updates the 'title' state
	handleTitleChange = event => {
		let title = this.state.title;
		title.content = event.target.value;

		this.setState({
			title: title
		});
	};

	//grabs the title and body state and updates the preview state.
	//opens the preview modal
	renderPreview() {
		let title = this.state.title;
		let body = this.state.body;

		// console.log(body);

		title = title.opentag + title.content + title.closetag;
		this.setState({
			preview: title + body,
			showPreviewModal: true
		});
	}

	handleClosePreviewModal = () => {
		this.setState({ showPreviewModal: false });
	};

	handleOpenSubmitModal = () => {
		this.setState({ showSubmitModal: true });
	};
	handleCloseSubmitModal = () => {
		this.setState({ showSubmitModal: false });
	};

	handleOpenUpdateModal = () => {
		this.setState({ showUpdateModal: true });
	};
	handleCloseUpdateModal = () => {
		this.setState({ showUpdateModal: false });
	};

	handleOpenImageModal = () => {
		this.setState({ showImageModal: true });
	};
	handleCloseImageModal = () => {
		this.setState({ showImageModal: false });
	};

	handleOpenYoutubeModal = () => {
		this.setState({ showYoutubeModal: true });
	};
	handleCloseYoutubeModal = () => {
		this.setState({ showYoutubeModal: false });
	};

	/**
	 * grabs the title from state and the contents of the editor and stores them in an object called 'content'
	 * @returns {Object} content = {title, body}
	 */
	getEditorContent() {
		if (this.state.editor) {
			const header = this.state.title;
			const body = this.state.editor.getContent();

			let content = {
				title: header.content,
				body: body
			};

			return content;
		}
		return null;
	}

	/**
	 * This function is executed if the user is editing a post.
	 * The new content from the editor is obtained and used to update the old post.
	 * Once the update is successful, the user is navigated to the 'posts' page
	 */
	handleUpdate = () => {
		let content = this.getEditorContent();

		let postData = this.getPostDatafromRoute();
		if (!postData) {
			console.log("there is no post Data");
			return;
		}

		let postID = this.getPostIdfromRoute();
		if (!postID) {
			console.log("there is no post ID");
			return;
		}

		this.database
			.updatePost(postData, content, postID)
			.then(result => {
				if (this.props.location.pathname.includes("editor")) {
					//navigate to the updated post
					this.setState({
						redirect: true,
						path: "/posts/" + this.props.location.state.postID
					});
				} else {
					this.setState({
						redirect: true,
						path: "/"
					});
				}
			})
			.catch(error => {
				console.log(error);
			});
	};

	/**
	 * This function is executed if the user is creatig a new post.
	 * The contents of the editor are obtained and submitted to the backend.
	 * On a successful submission, the user is redirected to the 'posts' page.
	 */
	handleSubmit = () => {
		if (this.state.editor) {
			let content = this.getEditorContent();

			if (content) {
				this.database
					.createPost(content)
					.then(result => {
						console.log("New post successfully created.");

						if (this.props.location.pathname.includes("editor")) {
							//navigate to the new post
							this.setState({
								redirect: true,
								path: "/posts/" + result
							});
						} else {
							this.setState({
								redirect: true,
								path: "/editor"
							});
						}
					})
					.catch(error => {
						console.log(error);
					});
			} else {
				console.log("Error: content returned null");
			}
		}
	};

	/**
	 * this function accepts a string value from a textinput field.
	 * The string must be the url/link for a photo. The link is then attached to a string
	 * containing custom html. This html ensures the photo will scale down and not up
	 * depending on the screen size of the device.
	 */
	handleImageTextInput = event => {
		let code = `<img src="${event.target.value}" alt="photo" style="display:block;
		margin-left:auto;
		margin-right:auto;
		max-width: 100%;
    	height: auto;"/>`;
		this.setState({
			image: code
		});
	};

	/**
	 * this function accepts file input from a fileinput field.
	 * The file and imagePreviewUrl state are updated.
	 */
	handleImageFileInput = event => {
		event.preventDefault();
		let reader = new FileReader();
		let file = event.target.files[0];

		reader.onloadend = () => {
			this.setState({
				file: file,
				imagePreviewUrl: reader.result
			});
		};

		reader.readAsDataURL(file);
	};

	/**
	 * @returns {JSX Element} If the imagePreviewUrl state is not
	 * empty an img element is returned with the value. A div with
	 * a message is retuned otherwise.
	 */
	renderImagePreview = () => {
		let url = this.state.imagePreviewUrl;
		if (url) {
			return (
				<img
					src={url}
					alt="preview"
					style={{
						display: "block",
						marginLeft: "auto",
						marginRight: "auto",
						maxWidth: "100%",
						height: "auto"
					}}
				/>
			);
		} else {
			return <div>Please select an image</div>;
		}
	};

	/**
	 * This function responds to an onClick event.
	 * The image state is inserted into the body of the editor.
	 */
	handleInsertImage = () => {
		let image = this.state.image;
		let content = this.state.editor.getContent();
		this.state.editor.setContent(content + image);

		this.setState({
			body: content + image
		});

		this.handleCloseImageModal();
	};

	/**
	 * This function responds to an onClick event.
	 * The file state is submitted to the backend to be uploaded.
	 * On success, the file is inserted into the editor by updating the image
	 * and body state.
	 */
	handleUploadImage = () => {
		this.database.uploadPhoto(this.state.file).then(result => {
			let code = `<img src="${result}" alt="photo" style="display:block;
			margin-left:auto;
			margin-right:auto;
			max-width: 100%;
    		height: auto;"/>`;

			let content = this.state.editor.getContent();
			this.state.editor.setContent(content + code);

			this.setState({
				image: code,
				body: content + code
			});

			this.handleCloseImageModal();
		});
	};

	/**
	 * this function accepts text input from a textinput field.
	 * The text must contain a url/link to a youtube video.
	 * The link state is updated.
	 */
	handleYoutubeTextInput = event => {
		this.setState({
			link: event.target.value
		});
	};

	/**
	 * This function responds to an onClick event.
	 * The link state value is attached to a string which contains
	 * html to embed the link. The embeded video size will scale up and down.
	 * The body state is updated.
	 */
	handleYoutubeSubmit = () => {
		let link = this.state.link;
		let regexp = /watch\?v=/gi;
		link = link.replace(regexp, "embed/");
		link = `
				<p>&nbsp;</p>		
				<p style="position: relative;width: 100%;height: 0; padding-bottom: 56.25%;">
					<iframe 
						style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;"
						src="${link}"
						allowfullscreen="allowfullscreen"
					>
					</iframe>
				</p>
				<p>&nbsp;</p>`;
		let content = this.state.editor.getContent();
		this.state.editor.setContent(content + link);

		this.setState({
			body: content + link
		});

		this.handleCloseYoutubeModal();
	};

	/**
	 * The getPost... functions are helper functions for when a post is being edited/updated
	 */
	getPostIdfromRoute() {
		if (this.props.location.state) if (this.props.location.state.postID) return this.props.location.state.postID;
	}

	getPostBodyfromRoute() {
		if (this.props.location.state)
			if (this.props.location.state.postData) return this.props.location.state.postData.body;
	}

	getPostDatafromRoute() {
		if (this.props.location.state) return this.props.location.state.postData;
	}

	render() {
		if (this.state.redirect) {
			return (
				<Redirect
					push
					to={{
						pathname: this.state.path,
						editable: this.props.editable
					}}
				/>
			);
		}

		return (
			<Container>
				<EditorContainer>
					<TextInput
						placeholder="Title of article"
						onChange={this.handleTitleChange}
						value={this.state.title.content}
					/>

					<textarea id={this.props.id} value={this.getPostBodyfromRoute()} onChange={e => console.log(e)} />
				</EditorContainer>

				<Toolbar>
					<Tool onClick={() => this.renderPreview()}>
						<ToolIcon className="material-icons">visibility</ToolIcon>
						<ToolText> PREVIEW </ToolText>
					</Tool>
					<Tool onClick={() => this.handleOpenImageModal()}>
						<ToolIcon className="material-icons">insert_photo</ToolIcon>
						<ToolText> IMAGE </ToolText>
					</Tool>
					<Tool onClick={() => this.handleOpenYoutubeModal()}>
						<ToolIcon className="material-icons">ondemand_video</ToolIcon>
						<ToolText> YOUTUBE </ToolText>
					</Tool>
					{!this.state.isEditing ? (
						<Tool onClick={this.handleOpenSubmitModal}>
							<ToolIcon className="material-icons">done_all</ToolIcon>
							<ToolText> SUBMIT </ToolText>
						</Tool>
					) : (
						<Tool onClick={this.handleOpenUpdateModal}>
							<ToolIcon className="material-icons">update</ToolIcon>
							<ToolText> UPDATE </ToolText>
						</Tool>
					)}
				</Toolbar>

				<ReactModal
					isOpen={this.state.showPreviewModal}
					contentLabel="Editor Preview"
					ariaHideApp={false}
					onRequestClose={this.handleClosePreviewModal}
					shouldCloseOnEsc={true}
					style={customStyles}
				>
					<CloseButton onClick={this.handleClosePreviewModal}>&times;</CloseButton>
					<div dangerouslySetInnerHTML={{ __html: this.state.preview }} />
				</ReactModal>

				<ReactModal
					isOpen={this.state.showImageModal}
					contentLabel="Editor Image"
					ariaHideApp={false}
					onRequestClose={this.handleCloseImageModal}
					shouldCloseOnEsc={true}
					style={customStyles}
				>
					<CloseButton onClick={this.handleCloseImageModal}>&times;</CloseButton>
					<ImageModalContainer>
						<ImageModalTitleText>Insert image url:</ImageModalTitleText>
						<input onChange={this.handleImageTextInput} />
						{this.state.image ? (
							<ImageModalButton onClick={this.handleInsertImage}>add image</ImageModalButton>
						) : null}

						<div style={{ margin: "2em" }} />
						<ImageModalTitleText>Upload image from PC:</ImageModalTitleText>
						<input className="fileInput" type="file" onChange={this.handleImageFileInput} />

						<ImageModalPreview>{this.renderImagePreview()}</ImageModalPreview>
						{this.state.file ? (
							<ImageModalButton onClick={this.handleUploadImage}>upload</ImageModalButton>
						) : null}
					</ImageModalContainer>
				</ReactModal>

				<ReactModal
					isOpen={this.state.showYoutubeModal}
					contentLabel="Editor Youtube"
					ariaHideApp={false}
					onRequestClose={this.handleCloseYoutubeModal}
					shouldCloseOnEsc={true}
					style={customStyles}
				>
					<CloseButton onClick={this.handleCloseYoutubeModal}>&times;</CloseButton>
					<ImageModalContainer>
						<ImageModalTitleText>Insert youtube video link:</ImageModalTitleText>
						<input onChange={this.handleYoutubeTextInput} />
						{this.state.link ? (
							<ImageModalButton onClick={this.handleYoutubeSubmit}>add video</ImageModalButton>
						) : null}
					</ImageModalContainer>
				</ReactModal>

				<ConfirmBox
					modalState={this.state.showSubmitModal}
					message={"Are you sure you want to create this post?"}
					onSubmit={this.handleSubmit}
					onCancel={this.handleCloseSubmitModal}
					actionText={"Submit"}
				/>

				<ConfirmBox
					modalState={this.state.showUpdateModal}
					message={"Are you sure you want to update this post?"}
					onSubmit={this.handleUpdate}
					onCancel={this.handleCloseUpdateModal}
					actionText={"Update"}
				/>
			</Container>
		);
	}
}

const customStyles = {
	content: {
		top: "20%",
		left: "20%",
		right: "20%",
		bottom: "20%"
	}
};

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	background: ${props => props.theme.background};
	flex: auto;

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;
	margin-top: 3em;
	padding: 2em;
`;

const Toolbar = styled.div`
	@media only screen and (max-width: 600px) {
		flex-direction: column;
		align-items: stretch;
	}
	display: flex;
	justify-content: center;
	align-items: center;

	padding: 5% 0;
`;

const Tool = styled.div`
	@media only screen and (max-width: 600px) {
		margin: 2% 0;
	}
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 0 0.5em;
	margin: 0 1em;
	border: 1px solid grey;
	border-radius: 3px;
	cursor: pointer;
	color: ${props => props.theme.homeFG};
`;

const ToolText = styled.span`
	font-size: small;
	padding: 0 0.5em;
	font-weight: bold;
`;

const ToolIcon = styled.i`
	// padding: 0 5%;
`;

const TextInput = styled.textarea`
	margin: 0% 0% 2%;
	font-weight: bold;
	font-size: large;
	width: 100%;
`;

const EditorContainer = styled.div`
	border: 1px solid grey;
	padding: 2%;
`;

const CloseButton = styled.span`
	float: right;
	font-size: xx-large;
	font-weight: bold;
	color: #aaaaaa;

	&:hover {
	}

	&:hover {
		color: #000;
		text-decoration: none;
		cursor: pointer;
	}
`;

const ImageModalContainer = styled.div`
	display: flex;
	flex-direction: column;
`;

const ImageModalTitleText = styled.div`
	font-size: 1.5rem;
	margin: 0.5em 0;
	font-variant: small-caps;
`;

const ImageModalPreview = styled.div`
	margin: 1em;
`;

const ImageModalButton = styled.button`
	width: 50%;
	margin: 1em;
	align-self: center;
	font-variant: small-caps;
`;

export default AdminEditor;
