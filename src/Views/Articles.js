import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
import AdminMenu from "../Components/AdminMenu";
import loader from "../puff.svg";

/**
 * TODO:
 * - add more post data to the article
 */

import ReactGA from "react-ga";

class Articles extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: null,
			posts: null,
			endID: null,
			postsCount: null,
			loadMoreFlag: true,
			adminControlsFlag: false,
			isloading: false,
			isloadingMore: false
		};

		this.database = Firebase.instance;
		ReactGA.pageview("/articles");
	}

	/**
	 * before all posts are loaded, display the loading animation
	 */
	componentWillMount() {
		this.setState({
			isloading: true
		});

		this.loadPosts();
	}

	/**
	 * Get the posts from the backend and add them to this component.
	 */
	loadPosts = () => {
		return this.database.posts
			.then(result => {
				this.setState({
					data: result
				});
			})
			.then(() => {
				return this.database.postsCount().then(result => {
					this.setState({
						postsCount: result.postCount
					});
				});
			})
			.then(() => {
				return this.renderPostTitles();
			})
			.catch(error => {
				console.log(error);
			});
	};

	/**
	 * This function responds to an onClick event.
	 * The button loading indicator is displayed while posts are fetched
	 * from the backend. On success, the posts displayed on the screen are updated
	 * and the loading indicator is hidden.
	 */
	loadMore = () => {
		this.setState({
			isloadingMore: true
		});
		let id = this.state.endID;

		return this.database
			.loadMorePosts(id)
			.then(result => {
				return this.setState({
					data: result
				});
			})
			.then(() => {
				this.setState({
					isloadingMore: false
				});
				return this.renderPostTitles();
			});
	};

	/**
	 * Function accepts a date integer value and returns a formatted string.
	 * @param {Integer} date Date integer value
	 * @returns {String} 'day/month/year, hours:minutes am/pm'
	 */
	parseDate(date) {
		// let date = this.state.data[key].date_created;

		let formatted = new Date(date);

		let hours = formatted.getHours();
		let minutes = formatted.getMinutes();
		if (minutes < 10) minutes = "0" + minutes;

		let time = "";
		let ampm = "";
		if (hours >= 12) ampm = "pm";
		else ampm = "am";
		hours = hours % 12;
		if (hours === 0) hours = 12; //if hour ever becomes 0 reset to 12

		time = hours + ":" + minutes + " " + ampm;

		return `${formatted.getDate()}/${formatted.getMonth() + 1}/${formatted.getFullYear()}, ${time}`;
	}

	/**
	 * Updates the data state array with the post data
	 */
	renderPostTitles = () => {
		let data = this.state.data; //data is in acsending order
		let elements = []; //unshift instead of push for descending order
		let endID = null;
		let count = this.state.postsCount;
		let flag = false;

		for (let key in data) {
			elements.unshift(
				<Post key={key}>
					<div>
						<h2>
							<Link
								to={{
									pathname: `/posts/${key}`,
									state: { post: data[key] }
								}}
								style={{ color: "inherit" }}
							>
								{data[key].title}
							</Link>
						</h2>

						<p>Date created: {this.parseDate(data[key].date_created)}</p>
					</div>

					<AdminMenu
						postID={key}
						postData={data[key]}
						delete={true}
						edit={true}
						featured={true}
						{...this.props}
					/>
				</Post>
			);
		}

		//combining old data with new data
		let posts = this.state.posts;
		let list = elements;
		if (posts) {
			elements.shift();
			list = posts.concat(elements);
		}

		//grabbing the id of the last post fetched for backend query
		let postKeys = Object.keys(data);
		endID = data[postKeys[0]].date_created;

		//checking if there are any more posts to load
		if (list.length === count) flag = true;

		this.setState({
			posts: list,
			endID: endID,
			loadMoreFlag: flag,
			isloading: false
		});
	};

	/**
	 * This function adds separators between all of the posts
	 */
	renderPostsNeat() {
		let elements = [];
		elements = this.state.posts;
		let neat = [];

		if (!elements) return null;

		//inserting separators
		for (let i = 0; i < elements.length; i++) {
			neat.push(elements[i]);
			if (i !== elements.length - 1) {
				neat.push(<Separator key={i - 1} />);
			}
		}

		return neat;
	}

	render() {
		return (
			<ArticlesContainer shadow={this.state.isloading}>
				{this.state.isloading ? (
					<LoaderContainer>
						<LoaderImg src={loader} alt="loading" />
					</LoaderContainer>
				) : null}

				<PostContainer>{this.renderPostsNeat()}</PostContainer>

				{this.state.loadMoreFlag ? null : (
					<LoadMoreContainer>
						<ClearButton onClick={this.loadMore}>
							Load More
							{this.state.isloadingMore ? (
								<LoaderImg src={loader} alt="loading" size="15%" mask="none" />
							) : null}
						</ClearButton>
					</LoadMoreContainer>
				)}
			</ArticlesContainer>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const ArticlesContainer = styled.div.attrs({
	shadow: props => {
		if (props.shadow) return "0";
		else return "0em 2em 100px 5px #1c1c1ce8";
	}
})`
	@media only screen and (max-width: 600px) {
		margin: 2em 2em 0 2em;
		width: 100%;
	}

	animation-duration: 250ms;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;

	margin-top: 3em;
	box-shadow: ${props => props.shadow};
`;

const PostContainer = styled.div`
	background-color: ${props => props.theme.background};
`;

const LoaderContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: transparent;
`;

const LoaderImg = styled.img.attrs({
	width: props => props.size || "5em",
	mask: props => props.mask || "invert(100%)"
})`
	width: ${props => props.width};
	filter: ${props => props.mask};
`;

const Separator = styled.hr`
	width: 75%;
	border-bottom: solid 1px;

	@media only screen and (max-width: 600px) {
		width: 80%;
		border-bottom: none;
	}
`;

const Post = styled.div`
	background-color: ${props => props.theme.background};
	color: ${props => props.theme.foreground};

	@media only screen and (max-width: 600px) {
		padding: 3em 1em;
	}

	font-size: large;
	display: flex;
	justify-content: space-between;

	h2 {
		font-weight: bold;
		color: ${props => props.theme.text};
		margin: 0 0 0.1em 0;

		a {
			text-decoration: none;
			&:hover {
				text-decoration: underline;
			}
		}
	}

	p {
		@media only screen and (max-width: 600px) {
			text-align: left;
		}
		text-align: justify;
	}

	animation-duration: 250ms;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	padding: 10% 15%;
`;

const LoadMoreContainer = styled.div`
	animation-duration: 150ms;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	display: flex;
	justify-content: center;
	background-color: ${props => props.theme.background};
`;

const ClearButton = styled.div`
	font-size: 1.3em;
	padding: 1% 5%;
	border: 1px solid ${props => props.theme.homeFG};
	cursor: pointer;
	text-align: center;
	font-variant: all-small-caps;

	display: flex;
	justify-content: space-between;
	align-items: center;

	border-radius: 5px;
	margin-bottom: 2em;

	transition: box-shadow 250ms ease;

	&:hover {
		box-shadow: 0px 2px 0px 0px ${props => props.theme.homeFG};
	}
`;

export default Articles;
