import React, { Component } from "react";
import styled from "styled-components";
// import Navbar from '../Components/Navbar';

/**
 * TODO:
 * - donation button style update
 * - add how to section
 * - could prob use a carousel from :https://github.com/FormidableLabs/nuka-carousel
 */

// import ReactGA from 'react-ga';
// ReactGA.pageview('/donate');

class Donate extends Component {
	render() {
		return (
			<Container>
				{/* <Navbar editable={this.props.editable} /> */}

				{/* <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
					<input type="hidden" name="cmd" value="_s-xclick" />
					<input type="hidden" name="hosted_button_id" value="J68WXDGKW3WGW" />
					
					<ClearButton>Donate</ClearButton>
				</form> */}

				<Content>Details on how to donate coming soon.</Content>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	// background: transparent;
	// flex: auto;

	background: ${props => props.theme.background};

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;
	// margin-top: 3em;
	box-shadow: 0em 0em 100px 0px black;
`;

const Content = styled.div`
	background: ${props => props.theme.background};
	color: ${props => props.theme.text};

	margin: 10% 15%;
	padding: 5em 0 4em 0;

	font-size: larger;
	border-bottom: solid;
`;

export default Donate;
