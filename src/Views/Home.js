import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
// import SubscribeBox from "../Components/SubscribeBox";
import AdminMenu from "../Components/AdminMenu";
import bgimage from "../resources/monk-homebg.jpg";

/**
 * TODO:
 * - double check all setstate calls to ensure it would not fire on an unmounted component
 */

import ReactGA from "react-ga";

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			featured: null
		};

		this.database = Firebase.instance;
		ReactGA.pageview("/home");
	}

	componentWillMount() {
		this.grabFeatured();
	}

	/**
	 * Get the featured post from the backend and updated the featured state.
	 */
	grabFeatured = () => {
		return this.database.getFeaturedPosts().then(result => {
			if (result === null) return;
			this.setState({
				featured: result.post
			});
		});
	};

	/**
	 * This function allows child components to update the featured state
	 * @param {Object} data = {postID, title, body, date_created}
	 */
	updateFeatured(data) {
		this.setState({
			featured: data
		});
	}

	render() {
		const featured = this.state.featured;
		return (
			<Container>
				<div>
					<BannerText>Shubhdaan Foundation</BannerText>
				{/* <Banner/> */}
				<Banner src={bgimage} alt="banner image"/>

				</div>

				{featured ? (
					<FeaturedContainer>
						<FeaturedHeaderContainer>
							<FeaturedHeader>Featured Article</FeaturedHeader>

							<AdminMenu
								featuredID={featured.postID}
								delete={true}
								update={model => this.updateFeatured(model)}
								{...this.props}
							/>
						</FeaturedHeaderContainer>

						<FeaturedPost>
							<Link
								to={{
									pathname: `/posts/${featured.postID}`,
									state: { post: featured }
								}}
								style={{ color: "inherit" }}
							>
								{featured.title}
							</Link>
						</FeaturedPost>
					</FeaturedContainer>
				) : null}

				{/* <SubscribeBox /> */}

				<ButtonContainer>
					{/* <WelcomeButton>
						<Link
							to={{
								pathname: `/donate`
							}}
							style={{ color: "inherit", textDecoration: "inherit" }}
						>
							Donate
						</Link>
					</WelcomeButton> */}
					<WelcomeButton>
						<Link
							to={{
								pathname: `/articles`
							}}
							style={{ color: "inherit", textDecoration: "inherit" }}
						>
							Read Articles
						</Link>
					</WelcomeButton>
				</ButtonContainer>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	min-height: inherit;
	background-color: ${props => props.theme.homeFG};
	// background: linear-gradient(0deg, #d5b1614a 0%, ${props => props.theme.homeFG} 38%);
	display: flex;
	justify-content: center;
	flex-direction: column;

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 100%;
`;

// const Banner = styled.div`
// 	@media only screen and (max-width: 600px) {
// 		// font-variant-caps: all-petite-caps;
// 		// font-size: 4em;
// 		// text-align: center;
// 		background-size: cover;
// 		height: 7em;
// 	}
// 	display: flex;
// 	justify-content: center;
// 	color: ${props => props.theme.background};
// 	padding: 5% 0 50%;
// 	// font-size: 8vw;
// 	// font-variant-caps: small-caps;
// 	// font-weight: bold;

// 	background-image: url(${bgimage});
// 	background-repeat: no-repeat;
// 	background-size: contain;
// 	background-position: top;
// `;
const Banner = styled.img`
	@media only screen and (max-width: 600px) {
		height: 80vh;
    	object-fit: cover;
		object-position: -16rem;

	}
	width: 100%;
`;

const BannerText = styled.p`
	@media only screen and (max-width: 600px) {
		font-variant-caps: all-petite-caps;
		font-size: 4em;
		text-align: center;
	}
	font-size: 8vw;
	font-variant-caps: small-caps;
	font-weight: bold;

	position: absolute;
    width: 100%;
    text-align: center;
    filter: invert(1);
    mix-blend-mode: difference;
`;

const WelcomeButton = styled.button`
	@media only screen and (max-width: 600px) {
		margin: 0.5em 0;
		flex-grow: 1;
		// background: ${props => props.theme.homeFG};
	}

	font-size: 1.6em;
	margin: 2% 7%;
	padding: 0.3em 0em;
	border: 2px solid #f3f3f37a;
	border-radius: 3px;
	// background: ${props => props.theme.homeFG};
	background-color: transparent;
	color: ${props => props.theme.background};
	cursor: pointer;
	font-variant: all-petite-caps;
	font-weight: bold;
	width: 240px;
`;

const ButtonContainer = styled.div`
	@media only screen and (max-width: 600px) {
		justify-content: space-around;
		flex-flow: wrap;
	}

	display: flex;
	justify-content: center;
	padding: 5%;
	font-variant: all-petite-caps;
	background-color: transparent;

`;

const FeaturedContainer = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	// color: ${props => props.theme.homeFG};
	// background-color: ${props => props.theme.landingText};
	color: ${props => props.theme.background};
	background-color: transparent;
	padding: 5rem 0;

	animation-duration: 1s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const FeaturedHeaderContainer = styled.div`
	display: flex;
	align-self: center;
	align-items: flex-end;
`;

const FeaturedHeader = styled.p`
	@media only screen and (max-width: 600px) {
		margin: 1em;
		font-size: 1.8em;
	}
	font-size: 2em;
	align-self: center;
	margin: 2% 0 0 0;
	font-weight: bolder;
	letter-spacing: 3px;
`;

const FeaturedPost = styled.p`
	@media only screen and (max-width: 600px) {
		font-size: 1.5em;
		margin-left: 0;
		// text-align: justify;
		margin: 1em;
	}

	font-size: 1.8em;
	text-align: center;

	a {
		text-decoration: none;
		&:hover {
			text-decoration: underline;
		}
	}
`;

export default Home;
