import React, { Component } from "react";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
import { Redirect } from "react-router-dom";

/**
 * TODO:
 * - need to display error on authentication failure
 */

import ReactGA from "react-ga";

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: null,
			password: null,
			redirectToHome: false,
			error: null
		};

		this.database = Firebase.instance;
		ReactGA.pageview("/login");
	}

	/**
	 * This function accepts a string value from a textinput field.
	 * The string must be a valid email format. The email state is updated.
	 */
	handleEmailChange = event => {
		this.setState({
			email: event.target.value
		});
	};

	/**
	 * This function accepts a string value from a textinput field.
	 * The password state is updated.
	 */
	handlePasswordChange = event => {
		this.setState({
			password: event.target.value
		});
	};

	/**
	 * This function responds to an onClick event.
	 * The email and password state are submitted to the backend for validation.
	 * On success, the user is redirected to the homepage, else an error is displayed.
	 */
	handleSubmit = event => {
		event.preventDefault();

		let data = this.state;

		let user = {};
		user.userID = data.email;
		user.password = data.password;

		this.database
			.loginUser(user)
			.then(() => {
				//use the parent func to modify editable
				this.props.toggleAdminControls();
				this.setState({
					redirectToHome: true
				});
			})
			.catch(error => {
				console.log(error);
				let msg = "";

				if (error.code === "auth/wrong-password") msg = "The password you entered is incorrect.";
				else if (error.code === "auth/user-not-found") msg = "The username you entered is incorrect.";
				else msg = error.message;

				msg += "\nPlease try again.";

				this.setState({
					error: msg
				});
			});
	};

	render() {
		if (this.state.redirectToHome) {
			return (
				<Redirect
					push
					to={{
						pathname: "/"
					}}
				/>
			);
		}
		return (
			<Container>
				<CustomForm onSubmit={this.handleSubmit}>
					<div>
						<TextInput type="email" placeholder="Email" required onChange={this.handleEmailChange} />
					</div>

					<div>
						<TextInput
							type="password"
							placeholder="Password"
							required
							onChange={this.handlePasswordChange}
						/>
					</div>

					<div>
						<SubmitButton type="submit" value="Login" />
					</div>
				</CustomForm>

				<div>
					<ErrorMessage>{this.state.error}</ErrorMessage>
				</div>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-content: center;
	background-color: transparent;
	padding: 2%;

	animation-duration: 0.25s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const CustomForm = styled.form`
	text-align: center;
`;

const TextInput = styled.input`
	@media only screen and (max-width: 600px) {
		width: auto;
	}

	margin-top: 2em;
	padding: 2%;
	font-size: 1rem;
	letter-spacing: 2px;
	background-color: transparent;
	border: 0;
	border-bottom: 2px solid ${props => props.theme.background};
	color: ${props => props.theme.background};
	width: 18em;

	&:focus {
		outline: 0;
	}
`;

const SubmitButton = styled.input`
	font-size: 1.2em;
	margin-top: 2em;
	padding: 0.2em 4em;
	border: 2px solid ${props => props.theme.background};
	border-radius: 3px;
	background-color: ${props => props.theme.homeFG};
	color: ${props => props.theme.background};
	cursor: pointer;
	font-variant: all-small-caps;
	transition: background-color 250ms;

	&:hover {
		background-color: ${props => props.theme.background};
		color: ${props => props.theme.homeFG};
	}
`;

const ErrorMessage = styled.p`
	color: crimson;
	margin-top: 5em;
	text-align: center;
`;

export default Login;
