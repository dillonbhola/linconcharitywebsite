import React, { Component } from "react";
import styled from "styled-components";
import ReactGA from "react-ga";

ReactGA.pageview("/notFound");

class NotFound extends Component {
	render() {
		return (
			<Container>
				<Content>
					<i className="material-icons">error_outline</i>
					<div>The page you are looking for does not exist.</div>
				</Content>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	@media only screen and (max-width: 600px) {
		margin: 2em 2em 0 2em;
		width: 100%;
	}

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;
`;

const Content = styled.div`
	@media only screen and (max-width: 600px) {
		flex-direction: column;
		padding: 0;
		margin: 0% 10%;
		text-align: center;
	}
	color: ${props => props.theme.background};

	margin: 10% 15%;
	padding: 5em 0 4em 0;

	font-size: larger;
	display: flex;
	align-items: center;
	justify-content: center;

	i {
		font-size: 5em;
		padding: 0.3em;
	}
`;

export default NotFound;
