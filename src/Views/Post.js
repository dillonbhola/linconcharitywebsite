import React, { Component } from "react";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
import CommentBox from "../Components/CommentBox";
import AdminMenu from "../Components/AdminMenu";

/**
 * TODO:
 * - add more meaningful post data.
 */

import ReactGA from "react-ga";

class Post extends Component {
	constructor(props) {
		super(props);

		this.state = {
			displayCommentBox: false,
			post: null,
			postID: this.props.match.params.postID,
			date: null
		};

		this.datebase = Firebase.instance;
	}

	componentWillMount() {
		return this.loadPost();
	}

	/**
	 * Gets the post data from the backend using the postID in state.
	 */
	loadPost() {
		let postID = this.state.postID;

		if (this.props.location.state) {
			this.setState({
				post: this.props.location.state.post
			});
			ReactGA.pageview("/post/"+this.props.location.state.post.title);
		} else {
			return this.datebase.post(postID).then(result => {
				this.setState({
					post: result
				});
				ReactGA.pageview("/post/"+result.title);
			});
		}
	}

	/**
	 * This function responds to an onClick event.
	 * The displayCommentBox state is changed to show the appropiate conent.
	 */
	handleAddComment = () => {
		this.setState({
			displayCommentBox: true
		});
	};

	/**
	 * Function accepts a date integer value and returns a formatted string.
	 * @param {Integer} date Date integer value
	 * @returns {String} 'day/month/year, hours:minutes am/pm'
	 */
	parseDate(date) {
		let formatted = new Date(date);

		let hours = formatted.getHours();
		let minutes = formatted.getMinutes();
		if (minutes < 10) minutes = "0" + minutes;

		let time = "";
		let ampm = "";
		if (hours >= 12) ampm = "pm";
		else ampm = "am";
		hours = hours % 12;
		if (hours === 0) hours = 12; //if hour ever becomes 0 reset to 12

		time = hours + ":" + minutes + " " + ampm;

		return `${formatted.getDate()}/${formatted.getMonth() + 1}/${formatted.getFullYear()}, ${time}`;
	}

	/**
	 * Function returns a formatted string based on the date_created property of the post state
	 * @returns {String} ' Posted month day, year'
	 */
	renderPostDate() {
		let format = this.state.post.date_created;
		let date = new Date(format);
		let month = date.getMonth();
		let day = date.getDate();
		let year = date.getFullYear();

		let months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
		return `Posted ${months[parseInt(month, 10)]} ${day}, ${year}`;
	}

	/**
	 * @returns {Array<JSX>} array of jsx elements containing comment data relating to post
	 */
	renderComments() {
		if (!this.state.post) return;

		let elements = [];

		let comments = this.state.post.comments;

		if (comments) {
			for (let key in comments) {
				elements.push(
					<Comment key={key}>
						<ContentHeader>
							<CommentHeader>
								<CommentUser>{comments[key].user}</CommentUser>
								<CommentDetails>{this.parseDate(comments[key].date_created)}</CommentDetails>
							</CommentHeader>

							<AdminMenu postID={this.state.postID} commentID={key} delete={true} {...this.props} />
						</ContentHeader>

						<CommentContent>{comments[key].value}</CommentContent>
					</Comment>
				);
			}
		}

		return elements;
	}

	/**
	 * This function responds to an onClick event. It returns a jsx element
	 * based on the value in the display state.
	 */
	renderAddCommentButton() {
		if (!this.state.post) return;

		let display = this.state.displayCommentBox;

		if (display) {
			return <CommentBox postID={this.state.postID} />;
		} else {
			return <ClearButton onClick={this.handleAddComment}>Add a Comment</ClearButton>;
		}
	}

	render() {
		return (
			<Container>
				<Content>
					<ContentHeader>
						{this.state.post ? (
							<PostContainer>
								<PostTitle>{this.state.post.title}</PostTitle>
								<PostDate>{this.renderPostDate()}</PostDate>
							</PostContainer>
						) : (
							<div />
						)}

						<AdminMenu
							postID={this.state.postID}
							postData={this.state.post}
							delete={true}
							edit={true}
							featured={true}
							{...this.props}
						/>
					</ContentHeader>

					{this.state.post ? <div dangerouslySetInnerHTML={{ __html: this.state.post.body }} /> : <div />}
				</Content>

				{this.state.post && this.state.post.comments ? (
					<Header>Comments ( {Object.keys(this.state.post.comments).length} )</Header>
				) : null}

				<CommentsContainer>{this.renderComments()}</CommentsContainer>

				<ClearButtonContainer>{this.renderAddCommentButton()}</ClearButtonContainer>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	background: ${props => props.theme.background};

	@media only screen and (max-width: 600px) {
		width: 100%;
	}

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;

	width: 80%;
	margin-top: 3em;
	box-shadow: 0em 0em 100px 0px black;

	max-width: 1000px;
`;

const Content = styled.div`
	@media only screen and (max-width: 600px) {
		padding: 1em;
	}
	background: ${props => props.theme.background};
	color: ${props => props.theme.text};
	padding: 2em;
	font-size: larger;

	p {
		@media only screen and (max-width: 600px) {
			font-size: initial;
			text-align: left;
		}
		text-align: justify;
		word-spacing: 0.2em;
	}
`;

const PostContainer = styled.div`
	margin: 0 0 2em 0;
`;

const PostTitle = styled.h2`
	@media only screen and (max-width: 600px) {
		font-size: 1.5rem;
	}
	font-weight: bold;
	font-size: 2em;
`;
const PostDate = styled.div`
	@media only screen and (max-width: 600px) {
		margin-top: 1em;
	}
	color: ${props => props.theme.details};
	font-size: 0.8em;
`;

const ContentHeader = styled.div`
	display: flex;
	justify-content: space-between;
`;

const Header = styled.div`
	@media only screen and (max-width: 600px) {
		font-size: 1.5rem;
	}
	padding: 4em 1em 0;
	background-color: ${props => props.theme.background};
	font-size: 1.8em;
`;

const CommentsContainer = styled.ul`
	@media only screen and (max-width: 600px) {
		padding: 2em;
	}
	list-style-type: none;

	background-color: ${props => props.theme.background};

	padding: 0em 2em 2em 5em;
	margin: 0;

	hr {
		border: 0;
		height: 1px;
		background-color: ${props => props.theme.foreground};
	}
`;
const CommentHeader = styled.div`
	@media only screen and (max-width: 600px) {
		display: initial;
	}
	display: flex;
	justify-content: center;
	align-items: baseline;
`;
const Comment = styled.li`
	margin-top: 2.5em;
`;
const CommentUser = styled.p`
	@media only screen and (max-width: 600px) {
		margin-bottom: 0;
	}
	font-weight: bold;
`;
const CommentDetails = styled.p`
	@media only screen and (max-width: 600px) {
		margin-top: 1em;
		margin-left: 0;
	}
	color: ${props => props.theme.details};
	font-size: 0.8em;
	margin-left: 2em;
`;
const CommentContent = styled.p`
	background-color: #3f3f3f1c;
	border-radius: 15px;
	padding: 1.5em;
	margin: 0;
	display: inline-flex;
`;

const ClearButtonContainer = styled.div`
	padding: 1em;
	background-color: ${props => props.theme.background};
`;
const ClearButton = styled.div`
	font-size: 1.3em;
	padding: 1em;
	font-variant: all-petite-caps;

	border-top: 1px solid ${props => props.theme.homeFG};
	cursor: pointer;
	text-align: center;
`;

export default Post;
