import React, { Component } from "react";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
import { Link } from "react-router-dom";
// import Navbar from '../Components/Navbar';

/**
 * TODO:
 * - decide on if the date info for the results should be indexed. That way the results would
 *   contain the date content. Or you can fetch the content again from firebase for each result.
 *   Might be better to go with the latter as it would be easy to transition to the POST page.
 * - add pagination.
 * - display error if no results are recieved
 * - add search bar.
 * - need to fix the text that is display when a search is goin on
 * - this really needs a loading icon
 * - prob try redoing search with promises instead
 * - when clicking on a result/hit, the user is navigated to the post page.
 * 	 if the post contains comments, they are not displayed. This can be remedied by
 * 	 having the post page load its contents differently.
 * - implement loader here
 */

class Search extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: null,
			results: null,
			hits: null,
			query: null
		};

		this.database = Firebase.instance;
	}

	componentWillMount() {
		console.log("will mount");
		if (!this.state.data) this.handleSearch();
	}

	// componentDidCatch() {
	// 	console.log("catch");
	// }

	// componentDidMount() {
	// 	console.log("did mount");
	// }

	//   componentWillMount() {
	//     console.log('will mount')
	//   }

	// componentWillUpdate() {
	// 	console.log("will update");
	// }

	// componentWillUnmount() {
	// 	console.log("unmount");
	// }

	componentDidUpdate() {
		console.log("did update");
		/**
		 * Since handleSearch is only triggered when the component mounts,
		 * it will not fire again if the component updates.
		 * This check is placed here in the event that the search is performed
		 * again from the navbar, which will cause this search component to update
		 */
		if (this.state.query !== this.props.location.search.substr(1)) {
			console.log("running new search");
			this.handleSearch();
		}
	}

	handleSearch = () => {
		let query = this.props.location.search.substr(1);
		this.database.search(query);

		let check = null;
		let runSearch = setInterval(() => {
			console.log("getting search data...");
			let results = this.database.getSearchData();
			if (results) {
				console.log(results);
				check = results;
				clearInterval(runSearch);
				this.setState({
					data: check,
					hits: check.total,
					query: query
				});
			}
		}, 500);
		setTimeout(() => {
			if (!check) {
				clearInterval(runSearch);
				console.log("forcing the stop");
			}
		}, 5000);
	};

	renderResults = () => {
		if (!this.state.data) return;

		let elements = [];
		let data = this.state.data.hits;

		data.forEach(post => {
			elements.unshift(
				<Post key={post._id}>
					<div>
						<h2>
							<Link
								to={{
									pathname: `/posts/${post._id}`,
									state: { post: post._source }
								}}
								style={{ color: "inherit" }}
							>
								{post._source.title}
							</Link>
						</h2>
					</div>
				</Post>
			);
		});

		// this.setState({
		//     results: elements
		// });
		return elements;
	};

	render() {
		return (
			<Container>
				{/* <Navbar editable={this.props.editable} /> */}

				<Content>
					{this.state.hits} Results for '{this.props.location.search.substr(1)}'
					{this.renderResults()}
				</Content>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	background: transparent;
	flex: auto;

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const Content = styled.div`
	background: ${props => props.theme.background};
	color: ${props => props.theme.foreground};

	margin: 10% 15%;
	padding: 5em 0 4em 0;

	font-size: larger;
	border-bottom: solid;
`;

const Post = styled.div`
	background: ${props => props.theme.background};
	color: ${props => props.theme.foreground};

	margin: 10% 15% 0% 15%;
	padding: 0 0 4em 0;
	font-size: large;

	display: flex;
	justify-content: space-between;

	h2 {
		font-weight: bold;
		color: ${props => props.theme.shadow};

		a {
			text-decoration: none;
			&:hover {
				text-decoration: underline;
			}
		}
	}

	p {
		text-align: justify;
	}
`;

export default Search;
