import React, { Component } from "react";
import styled from "styled-components";
import Firebase from "../Backend/Firebase";
// import Navbar from '../Components/Navbar';

/**
 * TODO:
 * - implement loader here
 */

class Validate extends Component {
	constructor(props) {
		super(props);

		this.state = {
			token: this.props.match.params.token,
			message: "Processing request..."
		};

		this.datebase = Firebase.instance;
	}

	componentWillMount() {
		//perfrom firebase check?
		console.log(this.props.match.params.token);
		let token = this.props.match.params.token;
		this.datebase.verifyEmail(token).then(results => {
			console.log(results);

			this.setState({
				message: results
			});
		});
	}

	render() {
		// console.log(this.props.match.params.token)
		return (
			<Container>
				{/* <Navbar editable={this.props.edi
                table} /> */}

				<Content>
					Thanks for subscribing. You will receive an email every time a new article is posted.
					<hr />
					{this.state.message}
				</Content>
			</Container>
		);
	}
}

/////////////////////////////////// styled components ///////////////////////////////////

const Container = styled.div`
	background: transparent;
	flex: auto;

	animation-duration: 0.5s;
	animation-name: fadeIn;
	animation-timing-function: ease-in;
`;

const Content = styled.div`
	background: ${props => props.theme.background};
	color: ${props => props.theme.foreground};

	margin: 10% 15%;
	padding: 5em 0 4em 0;

	font-size: larger;
	border-bottom: solid;
`;

export default Validate;
