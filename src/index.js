import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { ThemeProvider } from "styled-components";
import { BrowserRouter } from "react-router-dom";
import ScrollToTop from "./ScrollToTop";

const theme = {
	//white + grey
	shadow: "#566370",
	background: "ghostwhite",
	foreground: "lightslategrey",
	text: "black",
	details: "grey",
	homeFG: "#1d262d", //dark
	homeBG: "#f5f5f5", //light
	landingText: "#d5b161" //yellow
};

ReactDOM.render(
	<BrowserRouter>
		<ScrollToTop>
			<ThemeProvider theme={theme}>
				<App />
			</ThemeProvider>
		</ScrollToTop>
	</BrowserRouter>,
	document.getElementById("root")
);

registerServiceWorker();
